const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// create global css imports
// create admin css imports
// this gets imported in app and admin
mix.sass('resources/sass/admin.scss', 'public/css');
mix.sass('resources/sass/app.scss', 'public/css');

// build admin css
// mix
// .sass('app.scss')
// .sass([
//    'import.scss'
// ], 'public/.tmp/import.css')
// .scripts([
//   'app.js'
//   ], 'public/js/app.js')
// .scripts([
//   'jquery.min.js',
//   'bootstrap.min.js',
//   'parsley.min.js',
//   'underscore-min.js',
//   'unload-warning.js',
//   'sweetalert.min.js',
//   'handlebars.js',
//   'sumo-plugins/sumo-app-asset-helper.js'
//   ], 'public/js/all.js')
//   .scripts([
//       'import.css'
//   ], 'resources/assets/css/font-awesome-pro.css', 'public/.tmp')
// .styles([
//   // 'font-awesome.css',
//   'font-awesome-pro.css',
//   'sweetalert.css'
//   ], 'public/css/all.css');

// //===== ADMIN CSS AND JS
// mix
// .sass([
//   'admin-import.scss'
//   ], 'public/.tmp/admin-sass-import.css')
// .sass([
//   'admin.scss'
//   ], 'public/.tmp/admin-sass.css')
// .scripts([
//   'admin-sass-import.css',
//   'admin-sass.css'
//   ], 'public/css/admin.css', 'public/.tmp')
// .styles([
//   'dataTables.bootstrap.min.css',
//   'Jcrop.min.css',
//   'animate.min.css',
//   'bootstrap-toggle.css',
//   'redactor.css',
//   'sweetalert.css',
//   'jquery.fancybox.min.css',
//   'font-awesome.css'
//   ], 'public/css/admin-plugins.css')
// .scripts([
//   'jquery.min.js',
//   'bootstrap.min.js',
//   'jquery.dataTables.min.js',
//   'jquery.sumo-datepicker.min.js',
//   'dataTables.bootstrap.min.js',
//   'select2.full.min.js',
//   'parsley.min.js',
//   'underscore-min.js',
//   'jquery-ui.min.js',
//   'bootstrap-notify.min.js',
//   'Jcrop.min.js',
//   'handlebars.js',
//   'redactor.min.js',
//   'bootstrap-toggle.min.js',
//   'unload-warning.js',
//   'sweetalert.min.js',
//   'jquery.fancybox.min.js',
//   'sumo-admin.js',
//   'sumo-asset-manager.js'
//   ], 'public/js/admin.js');

// mix.task('livereload', 'public/css/app.css');

// mix.version([
//   'public/css/app.css',
//   'public/css/all.css',
//   'public/js/all.js',
//   'public/js/app.js'
// ]);

mix.browserSync('127.0.0.1:8000');
