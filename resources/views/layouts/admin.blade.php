<!DOCTYPE html>
<html>
<head>
  <title>Admin</title>
  <meta name="viewport" content="width=device-width">
  {!! Html::style('css/admin-plugins.css') !!}
  {!! Html::style('css/admin.css') !!}
  {!! Html::style('third_party/redactor/redactor.min.css') !!}
</head>
<body>
  <div id="page-loader" class="loader hide">
    <div class="display-table">
      <div class="display-cell">
        <div class="spinner"></div>
      </div>
    </div>
  </div>
  <nav class="navbar navbar-fixed-top">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">
          {{ HTML::image('img/admin/logo.png') }}
        </a>
      </div>
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav navbar-right">
          <li data-toggle="tooltip" data-placement="bottom"  title="Profile"><a href="{{route('adminProfile')}}"><i class="fa fa-user" ></i></a></li>
          <li data-toggle="tooltip" data-placement="bottom"  title="Logout"><a href="{{route('adminLogout')}}"><i class="fa fa-sign-out-alt"></i></a></li>
          <li data-toggle="tooltip" data-placement="bottom"  title="Menu"><a href="#" class="menu-btn"><i class="fa fa-bars"></i></a></li>
        </ul>
      </div>
    </div>
  </nav>
  <aside>
   <ul>
      <!-- TEMPLATE of DROPDOWN MENU
      <li>
        <span data-toggle="collapse"><i class="fa fa-gears"></i>Settings
          <i class="fa fa-chevron-circle-down collapse-icon"></i>
        </span>
        <ul class="collapse {{Menu::active('options', @$menu)}}">
          <li class="{{Menu::active('options-general', @$menu)}}"><a href="{{route('adminOptions',['category=general'])}}"><i class="fa fa-globe"></i>General</a></li>
          <li class="{{Menu::active('options-email', @$menu)}}"><a href="{{route('adminOptions',['category=email'])}}"><i class="fa fa-envelope"></i>Email Settings</a></li>
        </ul>
      </li>
      <li class="{{Menu::active('samples', @$menu)}}"><a href="{{route('adminSamples')}}"><i class="fa fa-user-secret"></i>Samples</a></li>
      -->
      <li class="{{Menu::active('dashboard', @$menu)}}"><a href="{{route('adminDashboard')}}"><i class="fa fa-home"></i>Dashboard</a></li>
      <li class="{{Menu::active('activities', @$menu)}}"><a href="{{route('adminActivities')}}"><i class="fa fa-suitcase"></i>Activities</a></li>
      <li class="{{Menu::active('articles', @$menu)}}"><a href="{{route('adminArticles')}}"><i class="fa fa-book"></i>Articles</a></li>
      <li class="{{Menu::active('banners', @$menu)}}"><a href="{{route('adminBanners')}}"><i class="fa fa-images"></i>Banners</a></li>
      <li class="{{Menu::active('page_categories', @$menu)}}"><a href="{{route('adminPageCategories')}}"><i class="fa fa-sitemap"></i>Page Categories</a></li>
      <li class="{{Menu::active('pages', @$menu)}}"><a href="{{route('adminPages')}}"><i class="fa fa-file"></i>Pages</a></li>
      <li class="{{Menu::active('users', @$menu)}}"><a href="{{route('adminUsers')}}"><i class="fa fa-users"></i>Users</a></li>
      <li class="{{Menu::active('user_roles', @$menu)}}"><a href="{{route('adminUserRoles')}}"><i class="fa fa-key"></i>User Roles</a></li>
      <li class="{{Menu::active('user_permissions', @$menu)}}"><a href="{{route('adminUserPermissions',[0])}}"><i class="fa fa-cog"></i>Functions</a></li>
      <li><a class="select asset-sidebar-link" data-toggle="modal" data-target="#assets-modal" href="#"><i class="fa fa-cubes"></i>Assets</a></li>
      <li>
        <span data-toggle="collapse" href="#options" aria-expanded="false" aria-controls="options"><i class="fa fa-gears"></i>Settings
          <i class="fa fa-chevron-circle-down collapse-icon"></i>
        </span>
        <ul id="options" class="collapse active {{Menu::active('options', @$menu)}}">
          <li class="{{Menu::active('options-general', @$menu)}}"><a href="{{route('adminOptions',['category=general'])}}"><i class="fa fa-globe"></i>General</a></li>
          <li class="{{Menu::active('options-email', @$menu)}}"><a href="{{route('adminOptions',['category=email'])}}"><i class="fa fa-envelope"></i>Email Settings</a></li>
        </ul>
      </li>
    </ul>
  </aside>
  <main>
    @yield('breadcrumbs')
    <div id="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-12">
            <div class="page-header">
              <h1>{{$title}}</h1>
            </div>
          </div>
        </div>
        <div class="row">
          @yield('content')
        </div>
      </div>
    </div>
  </main>
  @include('admin.modals.assets')
  @include('admin.modals.delete')
  @include('admin.templates.asset_image')
  {!! Html::script('js/admin.js') !!}
  {!! Html::script('third_party/redactor/redactor.min.js') !!}
  {!! Html::script('third_party/redactor/_plugins/table/table.min.js') !!}
  {!! Html::script('third_party/redactor/_plugins/fontcolor/fontcolor.min.js') !!}
  {!! Html::script('third_party/redactor/_plugins/fontsize/fontsize.min.js') !!}
  {!! Html::script('third_party/redactor/_plugins/alignment/alignment.min.js') !!}
  {!! Html::script('third_party/redactor/_plugins/counter/counter.min.js') !!}
  {!! Html::script('third_party/redactor/_plugins/widget/widget.min.js') !!}
  @yield('added-scripts')
</body>
</html>
